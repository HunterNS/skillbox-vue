export default function numberFormat(value) {
  if (isNaN(+value)) { return 0; }
  return new Intl.NumberFormat().format(value);
}
